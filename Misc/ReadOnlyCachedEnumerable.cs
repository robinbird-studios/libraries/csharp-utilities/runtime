using System;
using System.Collections.Generic;
using RobinBird.Utilities.Runtime.Extensions;

namespace RobinBird.Utilities.Runtime.Misc
{
	public readonly struct ReadOnlyCachedEnumerable<T>
	{
		private readonly List<T> pool;

		public int Count => pool.Count;
		
		public T this[int index] => pool[index];

		public ReadOnlyCachedEnumerable(List<T> pool)
		{
			this.pool = pool;
		}

		public int FindIndex(Predicate<T> value)
		{
			return pool.FindIndex(value);
		}

		public T Find(Predicate<T> match)
		{
			return pool.Find(match);
		}

		public bool IsNullOrEmpty()
		{
			return pool.IsNullOrEmpty();
		}

		public ReadOnlyCachedEnumerator<T> GetEnumerator()
		{
			return new ReadOnlyCachedEnumerator<T>(pool);
		}
	}
	
	public struct ReadOnlyCachedEnumerator<T>
	{
		private readonly List<T> pool;
		private int index;

		public ReadOnlyCachedEnumerator(List<T> pool)
		{
			this.pool = pool;
			index = 0;
		}

		public T Current
		{
			get
			{
				if (pool == null || index == 0)
					throw new InvalidOperationException();

				return pool[index - 1];
			}
		}

		public bool MoveNext()
		{
			index++;
			return pool != null && pool.Count >= index;
		}

		public void Reset()
		{
			index = 0;
		}
	}
	
	
	public readonly struct ReadOnlyCachedEnumerableConvert<TResult, TInput>
	{
		private readonly List<TInput> pool;

		private readonly Func<TInput, TResult> convertMethod;

		public int Count => pool.Count;
		
		public TResult this[int index] => convertMethod(pool[index]);

		public ReadOnlyCachedEnumerableConvert(List<TInput> pool, Func<TInput, TResult> convertMethod)
		{
			this.pool = pool;
			this.convertMethod = convertMethod;
		}

		public int FindIndex(Predicate<TResult> value)
		{
			var cm = convertMethod;
			return pool.FindIndex(input => value(cm(input)));
		}

		public ReadOnlyCachedEnumeratorConvert<TResult, TInput> GetEnumerator()
		{
			return new ReadOnlyCachedEnumeratorConvert<TResult, TInput>(pool, convertMethod);
		}
	}
	
	public struct ReadOnlyCachedEnumeratorConvert<TResult, TInput>
	{
		private readonly List<TInput> pool;
		private readonly Func<TInput, TResult> convertMethod;
		private int index;

		public ReadOnlyCachedEnumeratorConvert(List<TInput> pool, Func<TInput, TResult> convertMethod)
		{
			this.pool = pool;
			this.convertMethod = convertMethod;
			index = 0;
		}

		public TResult Current
		{
			get
			{
				if (pool == null || index == 0)
					throw new InvalidOperationException();

				return convertMethod(pool[index - 1]);
			}
		}

		public bool MoveNext()
		{
			index++;
			return pool != null && pool.Count >= index;
		}

		public void Reset()
		{
			index = 0;
		}
	}
}