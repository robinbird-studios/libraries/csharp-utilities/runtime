namespace RobinBird.Utilities.Runtime.Misc
{
	/// <summary>
	/// Empty Generic Type Argument
	/// </summary>
	public sealed class VoidType
	{
	}
}