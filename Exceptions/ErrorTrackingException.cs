using System;

namespace RobinBird.Utilities.Runtime.Exceptions
{
	public class ErrorTrackingException : Exception
	{
		public ErrorTrackingException(string name, string description, Exception innerException) : base(
			$"{name}\n{description}", innerException)
		{
		}
	}
}