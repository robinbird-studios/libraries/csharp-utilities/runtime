﻿#region Disclaimer

// <copyright file="TalosPool.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

using JetBrains.Annotations;
using RobinBird.Utilities.Runtime.Helper;

namespace RobinBird.Utilities.Runtime
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Pool to cache a collection of objects of type <typeparamref name="T" />. The developer
    /// can get or add objects to the pool. You must supply a create method so the pool can
    /// create new objects if required.
    /// </summary>
    /// <typeparam name="T">Type of the objects to pool</typeparam>
    public class Pool<T>
    {
        private readonly Stack<T> stack;
        private readonly List<T> activeObjects = new();
        private readonly string name;
        private readonly Func<object,T> createFunc;
        private readonly Action<T,object> setupFunc;
        private readonly Action<T, object> resetAction;
        private readonly object referenceObject;

        private IProfilerTrackingHandle<int> activeObjectCountCounter;
        private IProfilerTrackingHandle<int> ActiveObjectCountCounter
        {
            get
            {
                if (activeObjectCountCounter == null)
                {
                    activeObjectCountCounter = ProfilerTracking.CreateTrackingHandle<int>(name, "Pool",
                        ProfilerMarkerDataUnit.Count, ProfilerCounterOptions.FlushOnEndOfFrame);
                }
        
                return activeObjectCountCounter;
            }
        }


        /// <summary>
        /// Constructor for pool
        /// </summary>
        /// <param name="name">Used for Analytics and tracking</param>
        /// <param name="createFunc">Method to create pooled object.</param>
        /// <param name="resetAction">Method to call when object is returned to pool and has to be reset.</param>
        /// <param name="setupFunc">Get called whenever an item is retrieved from the pool. Also when it is newly created</param>
        public Pool(string name, Func<object,T> createFunc, [CanBeNull] Action<T,object> resetAction, [CanBeNull] Action<T, object> setupFunc = null)
        {
            if (createFunc == null)
            {
                throw new ArgumentNullException("createFunc", "Create method cannot be null");
            }
            //activeObjectCountCounter = ProfilerTracking.CreateTrackingHandle<int>(name, "Pool", ProfilerMarkerDataUnit.Count, ProfilerCounterOptions.FlushOnEndOfFrame);
            this.name = name;
            this.createFunc = createFunc;
            this.setupFunc = setupFunc;
            this.resetAction = resetAction;

            stack = new Stack<T>();
        }

        public Pool(string name, Func<T> createFunc, [CanBeNull] Action<T> resetAction, [CanBeNull] Action<T> setupFunc = null) :
            this(name, _ => createFunc(), (pooledObject, _) => resetAction?.Invoke(pooledObject), (pooledObject, _) => setupFunc?.Invoke(pooledObject))
        {
        }

        /// <summary>
        /// Constructor for pool
        /// </summary>
        /// <param name="referenceObject">Object that is passed with create and reset callbacks to use. Can be handy for creation on parent object.</param>
        /// <param name="name">Used for Analytics and tracking</param>
        /// <param name="createFunc">Method to create pooled object.</param>
        /// <param name="resetAction">Method to call when object is returned to pool and has to be reset.</param>
        /// <param name="setupFunc">Get called whenever an item is retrieved from the pool. Also when it is newly created</param>
        public Pool(string name, Func<object, T> createFunc, [CanBeNull] Action<T, object> resetAction, [CanBeNull] Action<T, object> setupFunc, object referenceObject)
            : this(name, createFunc, resetAction, setupFunc)
        {
            this.referenceObject = referenceObject;
        }

        public int PoolSize => stack.Count;

        public IReadOnlyList<T> ActiveObjects => activeObjects;

        public void ReturnAllActiveObjectsToPool()
        {
            for (int i = activeObjects.Count - 1; i >= 0; i--)
            {
                var activeObject = activeObjects[i];
                AddToPool(activeObject);
            }
        }

        /// <summary>
        /// Add item to the pool.
        /// </summary>
        public virtual void AddToPool(T item)
        {
            if (item == null)
            {
                return;
            }

            activeObjects.Remove(item);
            ActiveObjectCountCounter.Value--;
            resetAction?.Invoke(item, referenceObject);
            stack.Push(item);
        }

        /// <summary>
        /// Returns a item from the pool.
        /// If the pool is empty a new item will be created.
        /// </summary>
        public virtual T GetFromPool()
        {
            T result;
            if (stack.Count == 0)
            {
                result = createFunc(referenceObject);
            }
            else
            {
                result = stack.Pop();
            }
            activeObjects.Add(result);
            ActiveObjectCountCounter.Value++;
            setupFunc?.Invoke(result, referenceObject);
            return result;
        }
    }
}