using System;

namespace RobinBird.Utilities.Runtime.Flatbuffers.Extensions
{
	public static class GuidFlatExtensions
	{
		private static byte[] workingByteArray = new byte[16];
		public static Guid ToGuid(this GuidFlat guidFlat)
		{
			workingByteArray[0] = guidFlat.A;
			workingByteArray[1] = guidFlat.B;
			workingByteArray[2] = guidFlat.C;
			workingByteArray[3] = guidFlat.D;
			workingByteArray[4] = guidFlat.E;
			workingByteArray[5] = guidFlat.F;
			workingByteArray[6] = guidFlat.G;
			workingByteArray[7] = guidFlat.H;
			workingByteArray[8] = guidFlat.I;
			workingByteArray[9] = guidFlat.J;
			workingByteArray[10] = guidFlat.K;
			workingByteArray[11] = guidFlat.L;
			workingByteArray[12] = guidFlat.M;
			workingByteArray[13] = guidFlat.N;
			workingByteArray[14] = guidFlat.O;
			workingByteArray[15] = guidFlat.P;

			return new Guid(workingByteArray);
		}
	}
}