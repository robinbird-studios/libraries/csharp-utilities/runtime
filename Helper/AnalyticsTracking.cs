using System.Collections.Generic;
using JetBrains.Annotations;

namespace RobinBird.Utilities.Runtime.Helper
{
	public interface IAnalyticsTracker
	{
		void SendEvent(string name);

		void SendEvent(string name, string parameterName, string parameterValue);

		void SendEvent(string name, string parameterName, double parameterValue);

		void SendEvent(string name, string parameterName, long parameterValue);

		void SendEvent(string name, string parameterName, int parameterValue);

		void SendEvent(string name, params AnalyticsTracking.Parameter[] parameters);
	}
	
	public static class AnalyticsTracking
	{
		public struct Parameter
		{
			public string Name;
			[CanBeNull]
			public readonly string StringValue;
			public readonly long? LongValue;
			public readonly double? DoubleValue;

			public Parameter(string name, string value)
			{
				Name = name;
				StringValue = value;
				LongValue = null;
				DoubleValue = null;
			}
			
			public Parameter(string name, double value)
			{
				Name = name;
				StringValue = null;
				LongValue = null;
				DoubleValue = value;
			}
			
			public Parameter(string name, long value)
			{
				Name = name;
				StringValue = null;
				LongValue = value;
				DoubleValue = null;
			}
		}
		
		private static List<IAnalyticsTracker> trackers = new();
		
		public static void RegisterAnalyticsTracker(IAnalyticsTracker tracker)
		{
			trackers.Add(tracker);
		}

		public static void SendEvent(string name)
		{
			foreach (IAnalyticsTracker tracker in trackers)
			{
				tracker.SendEvent(name);
			}
		}

		public static void SendEvent(string name, string parameterName, string parameterValue)
		{
			foreach (IAnalyticsTracker tracker in trackers)
			{
				tracker.SendEvent(name, parameterName, parameterValue);
			}
		}
		
		public static void SendEvent(string name, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2)
		{
			foreach (IAnalyticsTracker tracker in trackers)
			{
				tracker.SendEvent(name, new Parameter(parameterName1, parameterValue1), new Parameter(parameterName2, parameterValue2));
			}
		}

		public static void SendEvent(string name, string parameterName, double parameterValue)
		{
			foreach (IAnalyticsTracker tracker in trackers)
			{
				tracker.SendEvent(name, parameterName, parameterValue);
			}
		}

		public static void SendEvent(string name, string parameterName, long parameterValue)
		{
			foreach (IAnalyticsTracker tracker in trackers)
			{
				tracker.SendEvent(name, parameterName, parameterValue);
			}
		}

		public static void SendEvent(string name, string parameterName, int parameterValue)
		{
			foreach (IAnalyticsTracker tracker in trackers)
			{
				tracker.SendEvent(name, parameterName, parameterValue);
			}
		}

		public static void SendEvent(string name, params Parameter[] parameters)
		{
			foreach (IAnalyticsTracker tracker in trackers)
			{
				tracker.SendEvent(name, parameters);
			}
		}
	}
}