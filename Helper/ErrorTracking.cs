using System.Collections.Generic;
using RobinBird.Utilities.Runtime.Exceptions;

namespace RobinBird.Utilities.Runtime.Helper
{
    using System;
    using System.Text;
    using Extensions;
    using Logging.Runtime;

    // TODO: Refactor this class to use Error Listeners. Too many things done at the same time here.
    public static class ErrorTracking
    {
        public const string LogCategory = "Crashlytics";
        private static readonly StringBuilder exceptionBuilder = new StringBuilder();

        private static readonly Type crashlyticsLoggedExceptionType;

#if ROBIN_BIRD_FIREBASE && !UNITY_WEBGL_TARGET
        private static bool isFirebaseInitialized;
#endif

        private class CachedException
        {
	        public string Name;
	        public string Description;
        }

        private static List<string> cachedLogContext = new List<string>();
        private static Dictionary<string, string> cachedCustomKeys = new Dictionary<string, string>();
        private static List<CachedException> cachedRecordExceptions = new List<CachedException>();

        public static void SetFirebaseInitialized()
        {
#if ROBIN_BIRD_FIREBASE && !UNITY_WEBGL_TARGET
	        Log.Info($"Got Firebase Initialize. Playing recorded context count: {cachedLogContext.Count.ToString()} and exception count: {cachedRecordExceptions.Count.ToString()}", category: LogCategory);
	        isFirebaseInitialized = true;
	        foreach (KeyValuePair<string,string> pair in cachedCustomKeys)
	        {
		        SetCrashlyticsCustomKey(pair.Key, pair.Value);
	        }
	        cachedCustomKeys.Clear();

	        foreach (string cachedLog in cachedLogContext)
	        {
		        LogCrashlyticsContext(cachedLog);
	        }
	        cachedLogContext.Clear();

	        foreach (CachedException cachedException in cachedRecordExceptions)
	        {
		        RecordCrashlyticsException(cachedException.Name, cachedException.Description);
	        }
	        cachedRecordExceptions.Clear();
#endif
        }

#if UNITY_2020_1_OR_NEWER && ROBIN_BIRD_FIREBASE && !UNITY_WEBGL_TARGET
        [UnityEngine.RuntimeInitializeOnLoadMethod(UnityEngine.RuntimeInitializeLoadType.SubsystemRegistration)]
        static void Reset()
        {
	        isFirebaseInitialized = false;
	        cachedLogContext = new List<string>();
	        cachedCustomKeys = new Dictionary<string, string>();
	        cachedRecordExceptions = new List<CachedException>();
        }
#endif

        static ErrorTracking()
        {
            crashlyticsLoggedExceptionType = AppDomain.CurrentDomain.GetType("Firebase.Crashlytics.LoggedException");
        }

        /// <summary>
        /// Record an Exception that should not happen.
        /// </summary>
        public static void RecordException(string name, string description = null, Exception innerException = null)
        {
            // exceptionBuilder.AppendLine(name);
            const string innerExceptionString = "--- INNER EXCEPTION ---";
            exceptionBuilder.AppendLine(description);
            if (innerException != null)
            {
                exceptionBuilder.AppendLine(innerExceptionString);
                exceptionBuilder.AppendLine(innerException.ToString());
            }
            var descriptionWithException = exceptionBuilder.ToString();
#if ROBIN_BIRD_FIREBASE && !UNITY_WEBGL_TARGET
            if (isFirebaseInitialized)
            {
	            RecordCrashlyticsException(name, descriptionWithException);
            }
            else
            {
                Log.Warn($"Would log exception but Crashlytics or Firebase not initialized.\nCaching for delayed reporting.\nName: {name}\nDescription: {descriptionWithException}", category: LogCategory);
                cachedRecordExceptions.Add(new CachedException()
                {
	                Name = name,
	                Description = descriptionWithException,
                });
            }
#endif
#if ROBIN_BIRD_SENTRY
            if (UnityEngine.Application.isEditor == false)
            {
                Sentry.SentrySdk.CaptureMessage($"{name}\n{descriptionWithException}");
            }
#endif
// #if NETCOREAPP
// 	        Sentry.SentrySdk.CaptureException(
// 		        new InvalidOperationException($"{name}\n{descriptionWithException}"));
// #endif

            Log.Error($"<b>{name}</b>\n{descriptionWithException}", category: LogCategory);
            exceptionBuilder.Clear();
            
#if NETCOREAPP
	        // In backend we actually want to throw these. No point of continuing after this.
	        // In Unity we try to handle errors and retry.
	        throw new ErrorTrackingException(name, description, innerException);
#endif
        }

#if ROBIN_BIRD_FIREBASE && !UNITY_WEBGL_TARGET
        private static void RecordCrashlyticsException(string name, string descriptionWithException)
        {
	        if (Firebase.Crashlytics.Crashlytics.IsCrashlyticsCollectionEnabled)
	        {
		        // Fake exception stacktrace so we have our own name at the top
		        exceptionBuilder.Clear();
		        exceptionBuilder.Append("  at ");
		        exceptionBuilder.Append(name);
		        exceptionBuilder.Append(".CustomException");
		        exceptionBuilder.AppendLine(" ()");

		        // Skip the frame of this method
		        var stackTrace = new System.Diagnostics.StackTrace(1).ToString();
		        exceptionBuilder.Append(stackTrace);
		        var stackTraceString = exceptionBuilder.ToString();

		        var crashlyticsException = CreateCrashlyticsLoggedException(name, descriptionWithException, stackTraceString);
		        Firebase.Crashlytics.Crashlytics.LogException(crashlyticsException);
	        }
	        else
	        {
		        Log.Warn("Would log exception but Firebase collection is not enabled", category: LogCategory);
	        }
        }
#endif

        private static Exception CreateCrashlyticsLoggedException(string name, string message, string stackTrace)
        {
            return (Exception)Activator.CreateInstance(crashlyticsLoggedExceptionType, name, message, stackTrace);
        }

        /// <summary>
        /// Log certain events that appear together with recorded exceptions by <see cref="RecordException"/>. Can be helpful
        /// to figure out what happened before the exception. Like breadcrumbs leading up to the exception
        /// </summary>
        public static void LogContext(string message)
        {
#if ROBIN_BIRD_FIREBASE && !UNITY_WEBGL_TARGET
            if (isFirebaseInitialized)
            {
	            LogCrashlyticsContext(message);
            }
            else
            {
                Log.Warn($"Would log context ({message}) but crashlytics not initialized", category: LogCategory);
		        cachedLogContext.Add(message);
            }
#endif
#if ROBIN_BIRD_SENTRY
            if (UnityEngine.Application.isEditor == false)
            {
                Sentry.SentrySdk.AddBreadcrumb(message);
            }
#endif
#if NETCOREAPP
	        Sentry.SentrySdk.AddBreadcrumb(message, "ErrorTracking", level: Sentry.BreadcrumbLevel.Info);
#endif
            Log.Info(message, category: LogCategory);
        }

        public static void SetCustomKey(string key, string value)
        {
#if ROBIN_BIRD_FIREBASE && !UNITY_WEBGL_TARGET
	        if (isFirebaseInitialized)
	        {
		        SetCrashlyticsCustomKey(key, value);
	        }
	        else
	        {
		        Log.Warn($"Would set custom key ({key}: {value}) but crashlytics not initialized", category: LogCategory);
		        cachedCustomKeys.Add(key, value);
	        }
#endif
#if NETCOREAPP
	        // TODO: Set Keys for backend Sentry?
#endif
        }

#if ROBIN_BIRD_FIREBASE && !UNITY_WEBGL_TARGET
        private static void LogCrashlyticsContext(string message)
        {
	        if (Firebase.Crashlytics.Crashlytics.IsCrashlyticsCollectionEnabled)
	        {
		        Firebase.Crashlytics.Crashlytics.Log(message);
	        }
	        else
	        {
		        Log.Warn($"Would log context ({message}) but Firebase collection is not enabled", category: LogCategory);
	        }
        }
#endif

#if ROBIN_BIRD_FIREBASE && !UNITY_WEBGL_TARGET
	    private static void SetCrashlyticsCustomKey(string key, string value)
	    {
		    if (Firebase.Crashlytics.Crashlytics.IsCrashlyticsCollectionEnabled)
		    {
			    Firebase.Crashlytics.Crashlytics.SetCustomKey(key, value);
		    }
		    else
		    {
			    Log.Warn($"Would set custom key ({key}: {value}) but Firebase collection is not enabled", category: LogCategory);
		    }
	    }
#endif
    }
}