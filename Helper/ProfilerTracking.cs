using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace RobinBird.Utilities.Runtime.Helper
{
	public enum ProfilerMarkerDataUnit : byte
	{
		/// <summary>
		///   <para>Use to display data value as string if ProfilerMarkerDataTypes.String16 or as a simple number without any unit abbreviations. Also use Undefined in combination with ProfilerMarkerDataTypes.Blob8 which won't be visualized.</para>
		/// </summary>
		Undefined,
		/// <summary>
		///   <para>Display data value as a time, specified in nanoseconds.</para>
		/// </summary>
		TimeNanoseconds,
		/// <summary>
		///   <para>Display data value as a size, specified in bytes.</para>
		/// </summary>
		Bytes,
		/// <summary>
		///   <para>Display data value as a simple number without any unit abbreviations.</para>
		/// </summary>
		Count,
		/// <summary>
		///   <para>Display data value as a percentage value with % postfix.</para>
		/// </summary>
		Percent,
		/// <summary>
		///   <para>Display data value as a frequency, specified in hertz.</para>
		/// </summary>
		FrequencyHz,
	}
	
	[Flags]
	public enum ProfilerCounterOptions : ushort
	{
		None = 0,
		FlushOnEndOfFrame = 2,
		ResetToZeroOnFlush = 4,
	}
	
	public interface IProfilerTrackingHandler
	{
		(Func<TValueType> getter, Action<TValueType> setter) CreateCounterHandle<TValueType>(string name, string category, ProfilerMarkerDataUnit unit, ProfilerCounterOptions counterOptions) where TValueType : unmanaged;
	}

	public interface IProfilerTrackingHandle<TValueType>
	{
		public TValueType Value {get; set;}
	}

	public class NoopTrackingHandle<TValueType> : IProfilerTrackingHandle<TValueType>
	{
		public TValueType Value
		{
			get => default;
			set { }
		}
	}

	public class ProfilerTrackingHandle<TValueType> : IProfilerTrackingHandle<TValueType>
	{
		private readonly Action<TValueType> valueSetHandler;
		private readonly Func<TValueType> valueGetHandler;
		public string Category;
		public string Name;

		public TValueType Value
		{
			get => valueGetHandler.Invoke();
			set => valueSetHandler.Invoke(value);
		}

		public ProfilerTrackingHandle(string category, string name, Action<TValueType> valueSetHandler, Func<TValueType> valueGetHandler)
		{
			Category = category;
			Name = name;
			this.valueSetHandler = valueSetHandler;
			this.valueGetHandler = valueGetHandler;
		}
	}
	
	public static class ProfilerTracking
	{
		private static readonly List<IProfilerTrackingHandler> Handlers = new();
		
		[Conditional("ENABLE_PROFILER")]
		public static void RegisterHandler(IProfilerTrackingHandler handler)
		{
			Handlers.Add(handler);
		}

		public static IProfilerTrackingHandle<TValueType> CreateTrackingHandle<TValueType>(string name, string category, ProfilerMarkerDataUnit unit, ProfilerCounterOptions counterOptions) where TValueType : unmanaged
		{
#if ENABLE_PROFILER
			var callbacks = Handlers[0].CreateCounterHandle<TValueType>(name, category, unit, counterOptions);

			return new ProfilerTrackingHandle<TValueType>(category, name, callbacks.setter, callbacks.getter);
#else
			return new NoopTrackingHandle<TValueType>();
#endif
		}
	}
}