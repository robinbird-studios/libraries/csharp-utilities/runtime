using System;
using FlatBuffers;
using RobinBird.Logging.Runtime;
using RobinBird.Utilities.Runtime.Extensions;

namespace RobinBird.Utilities.Runtime.Helper
{
    /// <summary>
    /// Serializable wrapper for System.Guid.
    /// Can be implicitly converted to/from System.Guid.
    ///
    /// Author: Katie Kennedy (Searous)
    /// </summary>
    [Serializable]
    public struct SerializableGuid 
#if UNITY_2022_1_OR_NEWER
        : UnityEngine.ISerializationCallbackReceiver
#endif
    {
        public const int ByteSize = 16;
        public static readonly SerializableGuid Empty = new SerializableGuid(Guid.Empty);
        
        private Guid guid;
        
#if UNITY_2022_1_OR_NEWER
        [UnityEngine.SerializeField]
#endif
        private string serializedString;
 
        public SerializableGuid(Guid guid) {
            this.guid = guid;
            serializedString = string.Empty;
        }
 
        public override bool Equals(object obj) {
            return obj is SerializableGuid compareGuid &&
                   guid.Equals(compareGuid.guid);
        }

        public bool Equals(SerializableGuid other)
        {
            return guid.Equals(other.guid);
        }

        public override int GetHashCode() {
            return guid.GetHashCode();
        }
 
        public void OnAfterDeserialize()
        {
            guid = GetTypedGuidFromString(serializedString);
        }

        private static Guid GetTypedGuidFromString(string guidString)
        {
            try
            {
                if (string.IsNullOrEmpty(guidString) == false)
                {
                    return Guid.ParseExact(guidString, "N");
                }
            } catch {
                Log.Error($"Attempted to parse invalid GUID string '{guidString}'. GUID will set to System.Guid.Empty");
            }

            return Guid.Empty;
        }

        public void OnBeforeSerialize()
        {
            serializedString = guid.ToString("N");
        }

        public bool IsEmpty() => guid == Guid.Empty;
 
        public override string ToString() => guid.ToString("N");
 
        public static bool operator ==(SerializableGuid a, SerializableGuid b) => a.guid == b.guid;
        public static bool operator !=(SerializableGuid a, SerializableGuid b) => a.guid != b.guid;
#if UNITY_EDITOR
        public static implicit operator SerializableGuid(UnityEditor.GUID guid) => new SerializableGuid(Guid.Parse(guid.ToString()));
        //public static implicit operator UnityEditor.GUID(SerializableGuid guid) => new UnityEditor.GUID(guid);
#endif
        public static implicit operator SerializableGuid(Guid guid) => new SerializableGuid(guid);
        public static implicit operator Guid(SerializableGuid serializable) => serializable.guid;
        public static implicit operator SerializableGuid(string serializedGuid) => new SerializableGuid(Guid.Parse(serializedGuid));
        public static implicit operator string(SerializableGuid serializedGuid) => serializedGuid.ToString();
        public static Offset<GuidFlat> Serialize(FlatBufferBuilder builder, SerializableGuid serializedGuid)
        {
            if (string.IsNullOrEmpty(serializedGuid.serializedString))
            {
                serializedGuid.OnBeforeSerialize();
            }

            var guid = GetTypedGuidFromString(serializedGuid.serializedString).ToByteArray();
            var guidOffset = GuidFlat.CreateGuidFlat(builder,
                A: guid[0],
                B: guid[1],
                C: guid[2],
                D: guid[3],
                E: guid[4],
                F: guid[5],
                G: guid[6],
                H: guid[7],
                I: guid[8],
                J: guid[9],
                K: guid[10],
                L: guid[11],
                M: guid[12],
                N: guid[13],
                O: guid[14],
                P: guid[15]
            );
            return guidOffset;
        }

        private static byte[] workingByteArray = new byte[16];
        public static implicit operator SerializableGuid(GuidFlat serializedGuid)
        {
            workingByteArray[0] = serializedGuid.A;
            workingByteArray[1] = serializedGuid.B;
            workingByteArray[2] = serializedGuid.C;
            workingByteArray[3] = serializedGuid.D;
            workingByteArray[4] = serializedGuid.E;
            workingByteArray[5] = serializedGuid.F;
            workingByteArray[6] = serializedGuid.G;
            workingByteArray[7] = serializedGuid.H;
            workingByteArray[8] = serializedGuid.I;
            workingByteArray[9] = serializedGuid.J;
            workingByteArray[10] = serializedGuid.K;
            workingByteArray[11] = serializedGuid.L;
            workingByteArray[12] = serializedGuid.M;
            workingByteArray[13] = serializedGuid.N;
            workingByteArray[14] = serializedGuid.O;
            workingByteArray[15] = serializedGuid.P;
            
            return new SerializableGuid(new Guid(workingByteArray));
        }
        
        public static implicit operator SerializableGuid(GuidFlat? serializedGuid)
        {
            return serializedGuid.HasValue == false ? new SerializableGuid(Guid.Empty) : serializedGuid.Value;
        }
    }
}